# Obligatorisk oppgave 1 - C-programmering med prosesser, tråder og synkronisering

Denne oppgaven består av følgende laboppgaver fra kompendiet:

* 4.5.b (Lage nye prosesser og enkel synkronisering av disse)
* 5.6.a (Lage nye tråder og enkel semafor-synkronisering av disse)
* 5.6.b (Flere Producere og Consumere)
* 6.10.a (Dining philosophers)

SE OPPGAVETEKST I KOMPENDIET. HUSK Å REDIGER TEKSTEN NEDENFOR!

## Gruppemedlemmer

**TODO: Erstatt med navn på gruppemedlemmene**

* Ola Eksempel
* Kari Mal

## Sjekkliste

* Har navnene på gruppemedlemmene blitt skrevet inn over?
* Har læringsassistenter og foreleser blitt lagt til med leserettigheter?
* Er issue-tracker aktivert?
* Er pipeline aktivert, og returnerer pipelinen "Successful"?
